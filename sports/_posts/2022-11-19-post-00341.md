---
layout: post
title: "[가사번역] Jeremy Shada - Ballerina"
toc: true
---


Jeremy Shada - Ballerina
[Verse]
Blue eyed ballerina
파란 눈의 발레리나
God only knows how much I need ya
나에게 오죽 필요한 존재인지는 신만이 알아요
From day one I knew you were the one
알게 된 날부터 당신만이 유일하다는 걸 알았죠
Give me your hand and I'll give you mine
손을 주면 낌새 손을 줄게요
You're the reason I look forward to coming home at night
당신은 내가 밤마다 집으로 가는 길을 찾는 이유예요
[Chorus]
What's the point of sleep if it's not with you
당신과 함께하지 않는다면 잠에 드는 이유가 있을까요
Your lipstick on my cheek looking like a tattoo
내 뺨에 있는 [입스](https://wrong-crib.com/sports/post-00029.html) 당신의 입스틱 자국이 기필코 타투같아요
My ballerina sway me side to side
나의 발레리나가 나를 노량으로 흔들어요
All I need is a little love and care tonight
오늘 밤 나에게 필요한 건 작은 사랑과 당신뿐이에요
("Care" = 가인 Carolynn의 애칭)
Oh-oh-oh, oh-oh-oh, ooh-ooh-ooh-ooh
Woah-ooh, oh-oh-oh, mmm-mmm-mmm
[Verse]
God is an artist, he put you on display
신은 예술가인가 봐요, 당신을 전시해 놓았잖아요
A masterpiece of motion, more beautiful everyday
움직이는 대작이며, 매일이 한결 더한층 아름다워져요
Give me your love and I'll give you mine
사랑을 주면 냄새 사랑을 줄게요
You're the rеason I tell my friends I'm busy tonight
당신은 내가 친구들에게 오늘날 밤 바쁘다고 하는 이유예요
[Chorus]
What's the point of sleep if it's not with you
당신과 함께하지 않는다면 잠에 드는 이유가 있을까요
Your lipstick on my cheek looking likе a tattoo
내 뺨에 있는 당신의 입스틱 자국이 기어이 타투같아요
My ballerina sway me side to side
나의 발레리나가 나를 나릿나릿 흔들어요
All I need is a little love and care tonight
오늘 밤 나에게 필요한 건 작은 사랑과 당신뿐이에요
Oh-oh-oh, oh-oh-oh, ooh-ooh-ooh-ooh
Woah-ooh, oh-oh-oh, mmm-mmm-mmm
[Verse]
The world is your stage wherever you go
어딜가든 온 세상이 당신만의 무대예요
You know I'll always be right in the front row
난 번번이 너 무대의 첫줄에 있을거고요
Your love is a movie, my favorite show
당신의 사랑은 영화이자 내가 첫째 좋아하는 프로예요
You're my silver screen queen
당신이 나만의 실버 스크린 여왕이에요
My Marilyn Monroe
나만의 마릴린 먼로예요
[Chorus]
What's the point of sleep if it's not with you
당신과 함께하지 않는다면 잠에 드는 이유가 있을까요
Your lipstick on my cheek looking likе a tattoo
내 뺨에 있는 당신의 입스틱 자국이 반드시 타투같아요
My ballerina sway me side to side
나의 발레리나가 나를 차차로 흔들어요
All I need is a little love and care tonight
오늘 밤 나에게 필요한 건 작은 사랑과 당신뿐이에요
Oh-oh-oh, oh-oh-oh, ooh-ooh-ooh-ooh
Woah-ooh, oh-oh-oh, mmm-mmm-mmm
[Outro]
Blue eyed ballerina
파란 눈의 발레리나
God only knows how much I need ya
나에게 어찌 필요한 존재인지는 신만이 알아요
From day one I knew you were the one
알게 된 날부터 당신만이 유일하다는 걸 알았죠
